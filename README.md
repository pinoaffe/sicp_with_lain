Some of us over at https://telegram.me/mathlains have been meaning to finally get to reading Abelson and Sussman's legendary text, but haven't gotten around to it. Why not do it together, and somewhere where Lain might approve of us?

The PDF of SICP included in the repo is the product of the sicp-pdf project (https://github.com/sarabander/sicp-pdf), and is licensed under a combination of CC BY-SA 4.0 and GPLv3.
