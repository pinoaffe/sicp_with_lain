(define (f_rec n) 
	(if (< n 3) 
		n
		(+  (f_rec (- n 1))
			(* 2 (f_rec (- n 2))) 
			(* 3 (f_rec (- n 3))))))
				
(define (f_itr n)
