(define (halve n) (/ n 2))
(define (double n) (* n 2))
(define (iseven n) (not(modulo n 2)))
(define (mul a b)
  (cond ((= b 0) 0)
		((iseven b) (double (mul a (halve b))))
		(else (+ a (mul a (- b 1))))))

