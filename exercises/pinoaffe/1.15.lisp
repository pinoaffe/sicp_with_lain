(define (p x)		(- (* 3 x) (* 4 x x x)))
(define (sine angle)
  	(if (not (> (abs angle) 0.1))
		angle
	(p (sine(/ angle 3.0)))))


a:	6 times
b:	the order of growth of the number of steps is log(a)*c, as every sine-call where a > 0.1 results in a single new instance of (sine) with a value a that is 3 times as small. As the value of a decreases exponentially, the number of steps is logarithmic.
The order of growth of the amount of space required is also logarithmic, for the amount of space required is directly linearly dependent on the amount of steps executed. 
