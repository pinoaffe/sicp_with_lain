because normal expressions are only evaluated once all of their sub-expressions have been evaluated, we don't need the operator of a normal expression untill the subexpressions have been evaluated and therefore the operator can be the result of an expression too.
In this case, during the evaluation of a-plus-abs-b, the first expression that is executed is (> b 0).
This returns a bool, signifying whether b is positive or negative (0 is grouped with negative because that doesnt matter).
If it's positive, a and b are added together, if it's negative, b is substracted from a. Thusly, the absolute value of b is added to a, and this is returned.
