(define (pascal r n) 
  	(if (or (= 1 n)
	        (= n r))
	     1
	     (+ (pascal (- r 1) n)
			(pascal (- r 1) (- n 1))
	     )
	 )
)

