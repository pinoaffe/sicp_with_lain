(define (+ a b)
	(if (= a 0) b (inc (+ (dec a) b))))
(+ 3 4)
(inc (+ (dec 3) 4))
(inc (inc (+ (dec (dec 3)) 4)))
(inc (inc (inc (+ (dec (dec (dec 3))) 4))))
(inc (inc (inc  4)))
(inc (inc 5))
(inc 6)
7

(+ 3 4)
(inc (+ (dec 3) 4))
(inc (inc (+ (dec 2) 4)))
(inc (inc (inc (+ (dec 1))) 4))))
(inc (inc (inc  4)))
(inc (inc 5))
(inc 6)
7
so the first definition of (+ a b) is recursive 



(define (+ a b)
	(if (= a 0) b (+ (dec a) (inc b))))
(+ 3 4)
(+ (dec 3) (inc 4))
(+ (dec 2) (inc 5))
(+ (dec 1) (inc 6))
(inc 7)
7
so the second definition of (+ a b) is iterative
