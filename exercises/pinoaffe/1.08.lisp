(define (gg? guess previous) 
	(> 0.001 (abs (- 1 
			 (/ guess previous)
		      )
		 )
	)
)
(define (cbrt g p n)
	(if 	(gg? g p)
		g
		(and 	(define newg (/ (+ (/ n g g) (* 2 g g g)) 3) )
			(cbrt newg g n)
		)
	)
)

