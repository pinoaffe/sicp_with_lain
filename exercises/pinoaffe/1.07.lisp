(define (gg? guess previous) 
	(> 0.001 (abs (- 1 
			 (/ guess previous)
		      )
		 )
	)
)
(define (sqrt g p n)
              (if (gg? g p)
                  g
                  (sqrt (/ (+ g (/ n g)) 2) g n)
              )
)

