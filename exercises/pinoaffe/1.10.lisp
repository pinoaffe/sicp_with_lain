(define (A x y)
	(cond 	((= y 0) 0)
			((= x 0) (* 2 y))
			((= y 1) 2)
			(else (A	(- x 1)
						(A 	x
							(- y 1))))))

(A 1 10)
1024

(A 2 4)
(A 1 (A 2 3))
(A 1 (A 1 (A 2 2)))
(A 1 (A 1 (A 1 (A 2 1))))
(A 1 (A 1 (A 1 2)))
(A 1 (A 1 4))
(A 1 16)
65536

(A 3 3)
65536

(define (f n) (A 0 n))
(define (alt_f n) (* 2 n))

(define (g n) (A 1 n))
(define (alt_g n) (expt 2 n))

(define (h n) (A 2 n))
(define (alt_h n) (n-1 times (expt 2 (expt 2 (e.....
