The interpreter gets stuck in an infinite loop, for in normal procedures the arguments are evaluated before the operator is applied to the arguments, which would mean that regardless of the results of new-if, there'd always be a branch executing (improve).
This would result in infinite recursion
